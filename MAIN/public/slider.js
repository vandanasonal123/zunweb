var valueBubble = '<output class="rangeslider__value-bubble" />';

var unit = $('input[type="range"]').attr('unit');

function updateValueBubble(pos, value, context) {
    pos = pos || context.position;
    value = value || context.value;
    var $valueBubble = $('.rangeslider__value-bubble', context.$range);
    var tempPosition = pos + context.grabPos;
    var position = (tempPosition <= context.handleDimension) ? context.handleDimension : (tempPosition >= context.maxHandlePos) ? context.maxHandlePos : tempPosition;

    if ($valueBubble.length) {
        var v = document.getElementsByClassName('rangeslider__value-bubble');
         if (value <= 3000 && value >= 2500) {
            document.getElementById('tempText1').style.color="white";
            document.getElementById('tempText1').innerHTML="YOUR BILL IS VERY HIGH, GET SOLAR!";
            document.getElementById('tempText1').style.visibility="hidden";
        } else if (value <= 20000 && value > 3000) {
            document.getElementById('tempText1').style.color="white";
			document.getElementById('tempText1').innerHTML="YOUR BILL IS VERY HIGH, GET SOLAR!";
            document.getElementById('tempText1').style.visibility="visible";
            
            // document.getElementById('tempText1').style.visibility="visible";
        } else if (value <= 40000 && value >= 20001) {
			document.getElementById('tempText1').innerHTML="SEEMS LIKE YOU NEED HELP, DON'T WORRY!";
            document.getElementById('tempText1').style.color="yellow";
			document.getElementById('tempText1').style.visibility="visible";
            
        } else if (value <= 60000 && value >= 40001) {
            document.getElementById('tempText1').innerHTML="CAN'T BELIEVE YOU'RE PAYING THIS MUCH!";
            document.getElementById('tempText1').style.color="yellow";
            document.getElementById('tempText1').style.visibility="visible";
        }else if (value <= 80000 && value >= 60001) {
           document.getElementById('tempText1').innerHTML="MUST GO SOLAR,YOU'RE PAYING A BOMB!";
           document.getElementById('tempText1').style.color="orange";
		   document.getElementById('tempText1').style.visibility="visible";
        }else if (value <= 100000 && value >= 80001) {
           document.getElementById('tempText1').innerHTML="ARE YOU SERIOUS? THAT'S ASTRONOMICAL!";
           document.getElementById('tempText1').style.color="orange";
           document.getElementById('tempText1').style.visibility="visible";
        }
        if (value <= 7000) {
            document.getElementById('rangeMinMax1').style.visibility = "hidden";

        }
        if (value > 7000 && value < 92000) {
            document.getElementById('rangeMinMax1').style.visibility = "hidden";
            document.getElementById('rangeMinMax2').style.visibility = "hidden";

        }
        if (value >= 92000) {

            document.getElementById('rangeMinMax2').style.visibility = "hidden";
        }
    }
    value = value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    $valueBubble[0].style.left = Math.ceil(position) + 'px';
    $valueBubble[0].innerHTML = " " + unit + value;
}

$('input[type="range"]').rangeslider({
    polyfill: false,
    onInit: function() {
        this.$range.append($(valueBubble));
        updateValueBubble(null, null, this);

    },
    onSlide: function(pos, value) {
        updateValueBubble(pos, value, this);

    }
});


 $(document).ready(function () {
    $(document).click(function (event) {
        var clickover = $(event.target);
        var _opened = $("#navbarSupportedContent").hasClass("navbar-collapse in");
        if (_opened === true && !clickover.hasClass("navbar-toggler")) {
            $("button.navbar-toggler").click();
        }
    });
});