var express       =     require("express"),
    app           =     express(),
    bodyParser    =     require("body-parser"),
    mysql         =     require("mysql"),
    path          =     require("path"),
    nodemailer    =     require('nodemailer'),
    fs            =      require("fs"),
    request       =      require("request"),
    smtpTransport =      require("nodemailer-smtp-transport"),
    handlebars    =      require("handlebars"),
    multer        =      require("multer");
    // lozad         =      require("lozad");


var connection =  mysql.createConnection({
     host : 'localhost',
     user : 'root',
     password : '',
     database : 'form'
});


// app.set("view engine","ejs");

app.use(express.static(__dirname + '/views'));
//Store all HTML files in view folder.
app.use(express.static(__dirname + '/public'));
//Store all JS and CSS in Public folder.
//Serves all the request which includes /images in the url from Images folder
app.use('/images', express.static(__dirname + '/Images'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true })); 

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});






connection.connect(function(err){
	if(!!err){
		console.log("Error");
	}else{
		console.log("Database Connected");
	}
});

/************************ MULTER *******************************/ 
var Storage = multer.diskStorage({
     destination: function(req, file, callback) {
         callback(null, "./Img");
     },
     filename: function(req, file, callback) {
         callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname);
     }
 });


 var upload = multer({
     storage: Storage
 }).array("imgUploader", 3);

/***********************************************************/


app.get("/",function(req,res){
	res.sendFile('index.html');


});

app.get("/pincode",function(req,res){
    console.log('request sent');
    console.log(req.query.pincode);
    var pincode = req.query.pincode;
var url ="http://postalpincode.in/api/pincode/" + pincode;

   console.log(url);
   var locationList=[];
   request(url, function(err, response, body){
       if(err){
        console.log(err);
          res.redirect("index.html");
       }else{
           var data = JSON.parse(body);
           console.log(data);
           console.log(data);
         var foundPincode =   data["PostOffice"].forEach(function(pcode){
            // locationList.push()
           // console.log(pcode["Name"]); 
           locationList.push(pcode["Name"]);

           });
             var jsonRes = JSON.stringify(locationList);
             res.setHeader('Content-Type', 'application/json');
               res.send(jsonRes);
           }
           
       });

});





app.post('/', function(req, res) {

console.log(req.body);
// res.write('You sent the name "' + req.body.fullname+'".\n');
// res.write('You sent the Email "' + req.body.email+'".\n');
// res.write('You sent the Phone "' + req.body.phone+'".\n');
// res.write('You sent the Pincode "' + req.body.pincode+'".\n');
// res.end();
 

var sql = "INSERT INTO users (fullname,email,phone,pincode,location,mnthbill) VALUES('"+req.body.fullname+"','"+req.body.email+"','"+req.body.phone+"','"+req.body.pincode+"','"+req.body.location+"','"+req.body.mnthbill+"')";

connection.query(sql,function(err, result)      
{  

  if (err)
     throw err;
  else 
  	console.log(result);
  res.redirect('congratulation.html');
});



// ***************************************************************8
// Emailer

   



var readHTMLFile = function(path, callback) {
    fs.readFile(path, {encoding: 'utf-8'}, function (err, html) {
        if (err) {
            throw err;
            callback(err);
        }
        else {
            callback(null, html);
        }
    });
};

smtpTransport = nodemailer.createTransport({
    service: 'gmail',
        host: 'smtp.gmail.com',
       
        auth: {
            user: 'zunroof123@gmail.com', // generated ethereal user
            pass: 'service12345'// generated ethereal password
        },
         tls: {
  	       rejectUnauthorized: false
         }
});

readHTMLFile(__dirname + '/views/emailer.html', function(err, html) {
    var template = handlebars.compile(html);
    var replacements = {
         fullname: `${req.body.fullname}`,
         phone: `${req.body.phone}`,
         pincode: `${req.body.pincode}`,
         mnthbill: `${req.body.mnthbill}`

    };
    var htmlToSend = template(replacements);
    var mailOptions = {
        from: 'zunroof123@gmail.com',
        to : `${req.body.email}`,
        subject : 'Registration',
        html : htmlToSend
     };
    smtpTransport.sendMail(mailOptions, function (error, response) {
        if (error) {
            console.log(error);
            // callback(error);
        }else{
        	console.log("Email Sent");
        }
    });
});
 

});


// ************************************ GO SOLAR *********************************************

app.get("/goSolar", function(req,res){
	res.sendFile("goSolar.html");
});
app.post("/goSolar",function(req,res){

console.log(req.body);
// res.write('You sent the name "' + req.body.name+'".\n');
// res.write('You sent the Email "' + req.body.email+'".\n');
// res.write('You sent the Phone "' + req.body.phone+'".\n');
// res.write('You sent the Pincode "' + req.body.pincode+'".\n');
// res.write('You sent the Date "' + req.body.datepicker+'".\n');
// res.write('You sent the Type "' + req.body.type+'".\n');
// res.end();

  
	var sql1 = "INSERT INTO assess (name,email,pincode,phone,datepicker,slot,type) VALUES('"+req.body.name+"','"+req.body.email+"','"+req.body.pincode+"','"+req.body.phone+"','"+req.body.datepicker+"','"+req.body.slot+"','"+req.body.type+"')";

     connection.query(sql1,function(err, result)      
		{  

		  if (err){
		  	console.log(err);
		     throw err;
		 }
		  else 
		  	console.log(result);
          res.redirect("congratulationSolar.html");
		});

     





// #### GoSolar EMAILER ########

var readHTMLFile = function(path, callback) {
    fs.readFile(path, {encoding: 'utf-8'}, function (err, html) {
        if (err) {
            throw err;
            callback(err);
        }
        else {
            callback(null, html);
        }
    });
};

smtpTransport = nodemailer.createTransport({
    service: 'gmail',
        host: 'smtp.gmail.com',
       
        auth: {
            user: 'zunroof123@gmail.com', // generated ethereal user
            pass: 'service12345'// generated ethereal password
        },
         tls: {
  	       rejectUnauthorized: false
         }
});

readHTMLFile(__dirname + '/views/assessEmailer.html', function(err, html) {
    var template = handlebars.compile(html);
    var replacements = {
         name: `${req.body.name}`,
         phone: `${req.body.phone}`,
         pincode: `${req.body.pincode}`,
         datepicker: `${req.body.datepicker}`,
         slot: `${req.body.slot}`,
         type: `${req.body.type}`

    };
    var htmlToSend = template(replacements);
    var mailOptions = {
        from: 'zunroof123@gmail.com',
        to : `${req.body.email}`,
        subject : 'Registration',
        html : htmlToSend
     };
    smtpTransport.sendMail(mailOptions, function (error, response) {
        if (error) {
            console.log(error);
            // callback(error);
        }else{
        	console.log("Email Sent");
        }

    });
});


     
});


// **************************** SOLAR CALCULATOR ******************************

app.get("/sCalculator",function(req,res){
	res.sendFile("sCalculator.html");
});

app.post("/sCalculator",function(req,res){
	console.log(req.body);



     // var APIKEY = "IGQYTqhExjd";
     // var MobileNo1 = req.body.phone;
     
     // var SenderID ="ZnRoof";
     // var Message = randomNumber();
     // var ServiceName = "PROMOTIONAL_SPL";

     // var url = "https://smsapi.24x7sms.com/api_2.0/SendSMS.aspx?APIKEY=" + APIKEY + "&MobileNo=" + MobileNo1  +"&SenderID=" +SenderID+ "&Message=" +Message+ "&ServiceName=" + ServiceName + "";
     // console.log(url);
     

//      request(url, function(err, response, body){
//         if(err){
//           console.log(err);
//        }else{
//            console.log("Message sent");
//         }
    
// });
var pincode = req.body.pincode;
var mnthbill = req.body.mnthbill;
var area = req.body.area;
var type = req.body.type;
var phone = req.body.phone;



    var sql3 = "INSERT INTO sCalculator (pincode,mnthbill,area,type,phone) VALUES('"+req.body.pincode+"','"+req.body.mnthbill+"','"+req.body.area+"','"+req.body.type+"','"+req.body.phone+"')";
    // var phone = req.body.phone;
   
    var sql4 = "INSERT INTO sCalculator1 (phone,otp) VALUES('"+req.body.phone+"','"+req.body.otp+"')";


  //    connection.query(sql3,function(err, result)      
		// {  

		//   if (err){
		//   	 throw err;
  //            console.log(result);
		//      }
  //            //adding other query to insert for varification table
  //                connection.query(sql4,function(err, result)      
  //                   {  

  //                     if (err){
  //                       throw err;
  //                    }

  //                     request(url, function(err, response, body){
  //                       if(err){
  //                         console.log(err);
  //                      }else{
  //                          console.log("Message sent");
  //                       }
                               
		//            });

  //          });
  //        });

     connection.query(sql3,function(err, result)      
        {  

          if (err){
            console.log(err);
             throw err;
         }
          else 
            console.log("result:"+result);
            console.log("res:"+res);
            res.writeHead(200, { 'Content-Type': 'text/html' });  
           res.send('done');
            // res.json('success');
            // res.sendFile("sCalculator.html");
        });

       
    // res.redirect("sCalculator.html");


})

// function randomNumber(){
//     return Math.floor(1000 + Math.random() * 9000);
// }
// function matchOtp(enteredOtp,originalOtp){
//   if(enteredOtp===originalOtp){
//     return true;
//   }
//   return false;
// }

// ############################################ Help ####################################

app.get("/help",function(req,res){
    res.sendFile("help.html");
});

app.post("/help",function(req,res){
    console.log(req.body);
// res.write('You sent the name "' + req.body.name+'".\n');
// res.write('You sent the Email "' + req.body.email+'".\n');
// res.write('You sent the Phone "' + req.body.phone+'".\n');
// res.write('You sent the Pincode "' + req.body.pincode+'".\n');
// res.write('You sent the Date "' + req.body.datepicker+'".\n');
// res.write('You sent the Type "' + req.body.type+'".\n');
// res.end();

  
    var sql = "INSERT INTO help (fullname,address,phone,quote_id,comment) VALUES('"+req.body.fullname+"','"+req.body.address+"','"+req.body.phone+"','"+req.body.quote_id+"','"+req.body.comment+"')";

     connection.query(sql,function(err, result)      
        {  

          if (err){
            console.log(err);
             throw err;
         }
          else 
            console.log(result);
        res.redirect("helpThank.html");
        });
     
     
    
 var readHTMLFile = function(path, callback) {
    fs.readFile(path, {encoding: 'utf-8'}, function (err, html) {
        if (err) {
            throw err;
            callback(err);
        }
        else {
            callback(null, html);
        }
    });
};

smtpTransport = nodemailer.createTransport({
    service: 'gmail',
        host: 'smtp.gmail.com',
       
        auth: {
            user: 'zunroof123@gmail.com', // generated ethereal user
            pass: 'service12345'// generated ethereal password
        },
         tls: {
           rejectUnauthorized: false
         }
});

readHTMLFile(__dirname + '/views/helpEmailer.html', function(err, html) {
    var template = handlebars.compile(html);
    var replacements = {
         fullname: `${req.body.fullname}`,
         addr: `${req.body.addr}`,
         phone: `${req.body.phone}`,
         quote_id: `${req.body.quote_id}`,
         comment: `${req.body.comment}`
         

    };
    var htmlToSend = template(replacements);
    var mailOptions = {
        from: 'zunroof123@gmail.com',
        to : `vandanasonal12391@gmail.com`,
        subject : 'Customer Help',
        html : htmlToSend
     };
    smtpTransport.sendMail(mailOptions, function (error, response) {
        if (error) {
            console.log(error);
            callback(error);
        }else{
            console.log("Email Sent");
        }

    });
});



});

app.get("/installerPlatform",function(req,res){
    res.sendFile("installerPlatform.html");
});

app.post("/installerPlatform",function(req,res){
    console.log(req.body);
// res.write('You sent the name "' + req.body.name+'".\n');
// res.write('You sent the Email "' + req.body.email+'".\n');
// res.write('You sent the Phone "' + req.body.phone+'".\n');
// res.write('You sent the Pincode "' + req.body.pincode+'".\n');
// res.write('You sent the Date "' + req.body.datepicker+'".\n');
// res.write('You sent the Type "' + req.body.type+'".\n');
// res.end();

  
    var sql = "INSERT INTO installer (fullname,phone,company,size,years) VALUES('"+req.body.fullname+"','"+req.body.phone+"','"+req.body.company+"','"+req.body.size+"','"+req.body.years+"')";

     connection.query(sql,function(err, result)      
        {  

          if (err){
            console.log(err);
             throw err;
         }
          else 
            console.log(result);
        res.redirect("installerThank.html");
        });

     

     var readHTMLFile = function(path, callback) {
    fs.readFile(path, {encoding: 'utf-8'}, function (err, html) {
        if (err) {
            throw err;
            callback(err);
        }
        else {
            callback(null, html);
        }
    });
};

smtpTransport = nodemailer.createTransport({
    service: 'gmail',
        host: 'smtp.gmail.com',
       
        auth: {
            user: 'zunroof123@gmail.com', // generated ethereal user
            pass: 'service12345'// generated ethereal password
        },
         tls: {
           rejectUnauthorized: false
         }
});

readHTMLFile(__dirname + '/views/installerEmailer.html', function(err, html) {
    var template = handlebars.compile(html);
    var replacements = {
         fullname: `${req.body.fullname}`,
         phone: `${req.body.phone}`,
         company: `${req.body.company}`,
         size: `${req.body.size}`,
         years: `${req.body.years}`
         

    };
    var htmlToSend = template(replacements);
    var mailOptions = {
        from: 'zunroof123@gmail.com',
        to : `vandanasonal12391@gmail.com`,
        subject : 'Customer Help',
        html : htmlToSend
     };
    smtpTransport.sendMail(mailOptions, function (error, response) {
        if (error) {
            console.log(error);
            callback(error);
        }else{
            console.log("Email Sent");
        }

    });
});



});


/********************************IMAGE UPLAOD************************************/ 
app.get("/upload",function(req,res){
    res.sendFile("/upload.html");
});

app.post("/api/Upload", function(req, res) {
     upload(req, res, function(err) {
         if (err) {
             return res.end("Something went wrong!");
         }
         return res.end("File uploaded sucessfully!");
     });
 });





app.listen(3000,process.env.IP,function(){
	console.log("Server Has Started");
});