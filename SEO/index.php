<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="Description" content="Make Money from Sun. Proven Solar System. Quality and subsidy assured by IITians">
  <meta name="Keywords" content="Delhi, solar, EPC, installer, rooftop, system, price, company, net, metering, subsidy, SECI, solar in delhi, Solar Panel, solar power, solar panel price in delhi, solar inverter, solar power plant, solar panel price in haryana, solar panel companies, solar inverter price list, solar company list in india, solar inverter price, Solar PV Installation, Solar Energy, solar in India, solar Rooftops, solar in Gurgaon, solar in NCR, solar zunroof">
  <meta name="author" content="ZunRoof Solar">
	<title>ZunRoof:Solar Plant for your home</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="seo.css">
</head>
<body>
   
	<nav class="navbar navbar-expand-lg navbar-light ">
				  <a class="navbar-brand" href="https://www.zunroof.com/">
				  	  <img id="brand-logo" src="images/logo.png" width="100" height="52" alt="">
				  </a>
				  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				    <span class="navbar-toggler-icon"></span>
				  </button>

				  <div class="collapse navbar-collapse" id="navbarSupportedContent">
				    <ul class="navbar-nav ml-auto">
				      <li class="nav-item ">
				        <a class="nav-link" href="https://www.zunroof.com/">Visit ZUNROOF.COM <span class="sr-only"></span></a>
				      </li>
				      <li class="nav-item ml">
				        <a class="nav-link" > | +91-920-956-9560</a>
				      </li>
				     
				      <li class="nav-item">
				        <a class="nav-link ">| GOSOLAR@ZUNROOF.COM</a>
				      </li>
				    </ul>
				    
				  </div>
	</nav>
    <hr> 
   
   
       <nav class= "navbar gosolar">
    	  <a href="https://www.zunroof.com/" class="nav-link"><button class="btn btn-lg solar-btn">GO SOLAR <br> Visit Zunroof.com</button></a>

      </nav>
        <div class="container">
    	    <div class="row videoframe">
    		    <div>
			        <iframe src="https://www.youtube.com/embed/-piXDEm7Sgw" allowfullscreen></iframe>
			     </div>
        </div>
     <!-- </div> -->
      

        <!-- <div class="container"> -->
  	     <div class=" row">
  	    	<div class="col-md-6">
  	    		<div class="card">
					  <div class="card-body">
					    <h5 class="card-title"> How To Read Electricity Bill In Delhi/NCR(BSES)</h5>
					     <br>
					    <p class="card-text">
                 
					    	 1. Name, Address and Phone number of connection holder. Please ensure your mobile number is correct as a lot of BSES services are available online and on their app.<br><br>
					    	2. Sanctioned Load: This is the maximum load you’re allowed to put on the grid. Monthly fixed charge in your bill is determined based on sanctioned load. Higher the sanctioned load, higher the fixed charges in the bill<br><br>
					    	3. Customer Account Number: This is a unique account number associated with your connection. You should use this number if you ever want to contact BSES. This can also be used to fetch your electricity bill details online.<br><br>
					    	4. Power Factor: A technical parameter calculated on the basis of your consumption pattern. You don’t need to worry about it as long as it is above 0.90. If it is below 0.90, get in touch with us, you’re basically paying at least ~20% extra electricity bill plus penalty charged by BSES. <b>Call us on +91-920-569-5690</b> <br><br>
					    	6. Meter type: Meters are either 1 phase or 3 phase.<br><br>
					    	7. Bill Basis: If a working meter was available for taking measurement, it would be on actual basis otherwise it would be pro-rata, based on your consumption history. <br><br>
					    	8. The table provides details of your consumption in current billing cycle based on your last 2 meter readings.<br>
					    	
              
						</p>
					    
					  </div>
					</div>
  	    	</div>

  	    	<div class="col-md-6">
  	    		<div class="card" >
					  <div class="card-body">
					    <h5 class="card-title"></h5>
					    
					    <p class="card-text">
					    	8. Electricity Charges:
					    	<ul>
					    		<li>
					    			Fixed Charge: This is based on sanctioned load and connection type.
					    		</li>
					    		<li>
					    			Electricity Units’ Charges: Cost of actual units consumed by the consumer. For residential customers, it is typically slab-based.
					    		</li>
					    		<li>
					    			Arrears: Any adjustment based on last bill charge and payment.
					    		</li>
					    		<li>
					    			Energy tax: There is an energy tax of 5% on Electricity Units’ Charges which every customer has to pay.
					    		</li>
					    		<li>
					    			Surcharge: This is 8% on Electricity Units’ Charges in Delhi/NCR. An average customer ends up paying 13% in taxes
					    		</li>
					    	</ul>
					    	9. Security Deposit: security deposit you paid when you applied for an electricity connection.<br><br>
					    	10. You can look into tariff structure for your connection. Domestic customers are charged 4 rupees/unit +13% tax up to 200 units, 5.95 rupees/unit +13% tax from 200-400 units, 7.30 rupees/unit +13% tax from 400-800 units, 8.10 rupees/unit +13% tax from 800-1200 units, effective rate is 9.15/unit, 8.75 rupees/unit +13% tax above 1200 units, effective rate is 9.90/unit <br><br>
					    	11. Historic consumption for past 6 months. If your electricity bill is higher than 2500 rupees consistently, you should consider going solar with ZunRoof. Simply fill out the form below or <a href="https://www.zunroof.com/">click here </a> or call +91-920-569-5690 and ZunRoof will do the rest<br><br><br>

						</p>
					    
					  </div>
					</div>
  	    	</div>
  	    </div>
    </div>
    <br>
  <div class="sub-footer">
  	<div class="container ">
    <div class="row">
    	
    		<div class="col-md-4" style="padding-top: 2rem; padding-bottom: 2rem; padding-left: 2rem;">
          <p class="sub-footer-links text-white ">Solar power plant</p>
          <p class="sub-footer-links text-white" >Solar in Delhi</p>
          <p class="sub-footer-links text-white" >Solar panel price in Haryana</p>
        </div>
        <div class="col-md-4" style="padding-top: 2rem; padding-bottom: 2rem">
          <p class="sub-footer-links text-white" >Solar PV Installation</p>
          <p class="sub-footer-links text-white" >Solar Rooftops</p>
          <p class="sub-footer-links text-white" >Solar Zunroof</p>
        </div>
        <div class="col-md-4" style="padding-top: 2rem; padding-bottom: 2rem">
          <p class="sub-footer-links text-white" >SECI</p>
          <p class="sub-footer-links text-white" >Solar inverter price</p>
          <p class="sub-footer-links text-white" >Solar company list in India</p>
        </div>
    		</div>
    		

    	</div>
    </div>
  </div>  
  
    <div class="footer">
    	<div class="container">
    		<h5 class="text-center" style="padding-top: 2rem;"> FOLLOW US </h5>
    	</div>
    	<div class="d-flex justify-content-center ">
    		<a class="connection" href="https://www.facebook.com/zunroof">
    			<img src="images/fb.png">
    		</a>
    		<a class="connection" href="https://www.twitter.com/zunroof">
    			<img src="images/twitter.png">
    		</a>
    		<a class="connection" href="https://www.linkedin.com/company/7947757/">
    			<img src="images/linkedin.png">
    		</a>
    		<a class="connection" href="https://plus.google.com/110929554712414479348">
    			<img src="images/google.png">
    		</a>
    		<a class="connection" href="https://www.youtube.com/channel/UC3eV9qJmNiH8hOjtrJJbHUw">
    			<img src="images/youtube.png">
    		</a>
          </div>

          <div class="container">
          	<hr>
          	<div class="row">
          	   <div class="col-md-6 contactus">
          	   	   <h4 > Contact Us</h4>
          	   	   <hr style="width:50%;">
          	   	   <p> TEL: +91-920-569-5690 | EMAIL: GOSOLAR@ZUNROOF.COM </p>
          	   	   
          	   	   <p> #581,DLF PHASE 5 <br> SECTOR 43,GURGAON <br> HARYANA 122009,INDIA </p>
          	   </div>
          	   <div class="col-md-6 maps">
          	   	   <div style= "margin: 25px auto;" id="map"></div>
          	   </div>
          	</div>
          </div>
    		
    	

    	
    
    </div>

    <script>
      function initMap() {
        var zunroof = {lat: 28.457975, lng: 77.095597};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 4,
          center: zunroof
        });
        var marker = new google.maps.Marker({
          position: zunroof,
          map: map
        });
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDAjX5N2c6o1HS0_DN0NVqjPiGMonb0ALk&callback=initMap">
    </script>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>
</html>